class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    gets.chomp.split(',').map {|el| Integer(el)}
  end

  def prompt
    puts 'Please select a target (like '3,4')'
  end

end
