class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
    @hit = false
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def attack(pos)
    if board[pos] == :s
      @hit = true
    else
      @hit = false
    end
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play
    unless game_over?
      play_turn
    end 
  end

  def play_turn
    player_move = player.get_play
    attack(player_move)
  end
# Prints information on the current state of the game,
# including board state and the number of ships remaining.
  def display_status

  end

end
