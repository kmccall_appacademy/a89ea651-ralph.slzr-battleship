class Board
  def self.default_grid
    Array.new(10){Array.new(10)}
  end

  attr_reader :grid

  def initialize(grid=self.class.default_grid, random = false)
    @grid = grid
    randomize if random
  end

  def count
    count = 0
    @grid.each {|row| count += row.count(:s)}
    count
  end

  def self.random
    self.new(self.default_grid, true)
  end

  def display
    header = (0..9).to_a.join(' ')
    p " #{header}"
    grid.each_with_index {|row, i| display_row(row, i)}
  end

  def display_row(row, i)
    chars = row.map {|el| DISPLAY_HASH[el]}.join(' ')
    p "#{i} #{chars}"
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    @grid[row][col] = val
  end

  def in_range?(pos)
    pos.all? {|x| x.between?(0, grid.length - 1)}
  end

  def empty?(pos=nil)
    if pos
      self[pos].nil?
    else
      count == 0
    end
  end

  def full?
    @grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise 'error' if full?
    pos = random_pos

    until empty?(pos)
      pos = random_pos
    end
    self[pos] = :s
  end

  def random_pos
    [rand(size), rand(size)]
  end

  def size
    grid.length
  end

  def won?
    @grid.flatten.none? {|el| el == :s}
  end

end
